﻿Imports System.Windows.Forms

Public Class Form1
    REM Help
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        System.Windows.Forms.MessageBox.Show("Press ""Open"" and select a file. It will then be corrupted and the program will ask you where to save the corrupted file, so don't worry about losing your data. ")
    End Sub

    REM Open
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim FO As OpenFileDialog = New OpenFileDialog()
        Select Case FO.ShowDialog()
            Case DialogResult.OK
                Button1.Enabled = False
                Dim Done As Byte() = Nothing
                UseWaitCursor = True
                Dim CTH As System.Threading.Thread = New Threading.Thread(
                    Sub()
                        Done = Corrupt(FO.FileName)
                        Dim Svf As SaveFileDialog = New SaveFileDialog()
                        Invoke(Sub()
                                   Dim Res As DialogResult = Svf.ShowDialog()
                                   Select Case Res
                                       Case DialogResult.OK
                                           System.IO.File.WriteAllBytes(Svf.FileName, Done)
                                           Svf = Nothing
                                           Done = Nothing
                                       Case Else
                                           System.Windows.Forms.MessageBox.Show("Result not saved.")
                                   End Select
                                   Button1.Enabled = True
                                   Text = "File Corruptor 15,000"
                                   ProgressBar1.Value = 0
                                   UseWaitCursor = False
                               End Sub)
                    End Sub)
                CTH.Start()
            Case Else
        End Select
    End Sub
    Private Function Corrupt(Filename As String) As Byte()
        Dim Result As Byte() = Nothing
        Result = System.IO.File.ReadAllBytes(Filename)
        Invoke(Sub()
                   ProgressBar1.Maximum = Result.Length
               End Sub)
        Dim RND As Random = New Random()

        For I = 0 To Result.Length
            Dim Yes As Int32 = I
            Dim StepSize As Int32 = RND.Next(RND.Next(10, 128))
            I += StepSize
            Invoke(Sub()
                       ProgressBar1.Value += StepSize
                       Text = Yes.ToString() + "B / " + Result.Length.ToString() + "B Done"
                   End Sub)
            If (RND.Next(0, 200) < 100) And I > 256 Then
                If (Result.Length > I) Then
                    Result(I) = RND.Next(0, 255)
                Else
                    Exit For
                End If
            End If
            If (Result.Length < I) Then
                Exit For
            End If
        Next

        Return Result
    End Function

    REM I can't be bothered to fix the thread issue
    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        System.Diagnostics.Process.GetCurrentProcess().Kill()
    End Sub
End Class
